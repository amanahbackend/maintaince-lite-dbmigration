//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GulfGroupDataMigration
{
    using System;
    using System.Collections.Generic;
    
    public partial class TechnicanUsedItem
    {
        public int Id { get; set; }
        public int Amount { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public System.DateTime DeletedDate { get; set; }
        public string FK_CreatedBy_Id { get; set; }
        public string FK_DeletedBy_Id { get; set; }
        public int FK_Order_Id { get; set; }
        public string FK_Technican_Id { get; set; }
        public string FK_UpdatedBy_Id { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsReleased { get; set; }
        public int ItemId { get; set; }
        public System.DateTime UpdatedDate { get; set; }
    }
}
