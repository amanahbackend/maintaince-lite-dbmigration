﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GulfGroupDataMigration
{
    class Program
    {
        static void Main(string[] args)
        {


            GulfStageEntities NewCustomerContext = new GulfStageEntities();

            GulfGroupOldCustomerDatabaseEntities OldCustomerContext = new GulfGroupOldCustomerDatabaseEntities();



          var OldExcelCustomers =    OldCustomerContext.OldCustomers.ToList();

            int CID = 0;

           foreach (var OldExcelCustomer in OldExcelCustomers)
            {


                Customer NewCustomer = new Customer();
                CID += 1;
                Console.WriteLine("CID is " + CID.ToString("D12"));

                NewCustomer.Name = OldExcelCustomer.Name;
                NewCustomer.CivilId = CID.ToString("D12");
                NewCustomer.FK_MachineType_Id = 1;
                NewCustomer.FK_CustomerType_Id = 1;
                NewCustomer.Remarks = "";
                NewCustomer.IsDeleted = false;
                NewCustomer.CreatedDate = DateTime.Now;
                NewCustomer.UpdatedDate = DateTime.Now;
                NewCustomer.DeletedDate = DateTime.Now;

                NewCustomerContext.Customers.Add(NewCustomer);
                NewCustomerContext.SaveChanges();
                Console.WriteLine("New Customer Added Called  : " + NewCustomer.Name + " With Id :" + NewCustomer.Id);


                if (!string.IsNullOrEmpty(OldExcelCustomer.Phone1))
                {
                    CustomerPhoneBook customerPhoneBook1 = new CustomerPhoneBook();

                    customerPhoneBook1.Phone = OldExcelCustomer.Phone1;
                    customerPhoneBook1.FK_Customer_Id = NewCustomer.Id;
                    customerPhoneBook1.IsDeleted = false;
                    customerPhoneBook1.CreatedDate = DateTime.Now;
                    customerPhoneBook1.UpdatedDate = DateTime.Now;
                    customerPhoneBook1.DeletedDate = DateTime.Now;
                    customerPhoneBook1.FK_PhoneType_Id = 1;

                    NewCustomerContext.CustomerPhoneBooks.Add(customerPhoneBook1);
                    NewCustomerContext.SaveChanges();
                    Console.WriteLine("New customerPhoneBook1 Added Called  : " + customerPhoneBook1.Phone + " With Id :" + customerPhoneBook1.Id);

                }




                if (!string.IsNullOrEmpty(OldExcelCustomer.Phone2.ToString()))
                {
                    CustomerPhoneBook customerPhoneBook2 = new CustomerPhoneBook();

                    customerPhoneBook2.Phone = OldExcelCustomer.Phone2.ToString();
                    customerPhoneBook2.FK_Customer_Id = NewCustomer.Id;
                    customerPhoneBook2.IsDeleted = false;
                    customerPhoneBook2.CreatedDate = DateTime.Now;
                    customerPhoneBook2.UpdatedDate = DateTime.Now;
                    customerPhoneBook2.DeletedDate = DateTime.Now;
                    customerPhoneBook2.FK_PhoneType_Id = 1;

                    NewCustomerContext.CustomerPhoneBooks.Add(customerPhoneBook2);
                    NewCustomerContext.SaveChanges();

                    Console.WriteLine("New customerPhoneBook2 Added Called  : " + customerPhoneBook2.Phone + " With Id :" + customerPhoneBook2.Id);

                }




                if (!string.IsNullOrEmpty(OldExcelCustomer.Phone3))
                {
                    CustomerPhoneBook customerPhoneBook3 = new CustomerPhoneBook();

                    customerPhoneBook3.Phone = OldExcelCustomer.Phone3;
                    customerPhoneBook3.FK_Customer_Id = NewCustomer.Id;
                    customerPhoneBook3.IsDeleted = false;
                    customerPhoneBook3.CreatedDate = DateTime.Now;
                    customerPhoneBook3.UpdatedDate = DateTime.Now;
                    customerPhoneBook3.DeletedDate = DateTime.Now;
                    customerPhoneBook3.FK_PhoneType_Id = 1;

                    NewCustomerContext.CustomerPhoneBooks.Add(customerPhoneBook3);
                    NewCustomerContext.SaveChanges();
                    Console.WriteLine("New customerPhoneBook3 Added Called  : " + customerPhoneBook3.Phone + " With Id :" + customerPhoneBook3.Id);

                }


                if (!string.IsNullOrEmpty(OldExcelCustomer.Phone4))
                {
                    CustomerPhoneBook customerPhoneBook4 = new CustomerPhoneBook();

                    customerPhoneBook4.Phone = OldExcelCustomer.Phone4;
                    customerPhoneBook4.FK_Customer_Id = NewCustomer.Id;
                    customerPhoneBook4.IsDeleted = false;
                    customerPhoneBook4.CreatedDate = DateTime.Now;
                    customerPhoneBook4.UpdatedDate = DateTime.Now;
                    customerPhoneBook4.DeletedDate = DateTime.Now;
                    customerPhoneBook4.FK_PhoneType_Id = 1;

                    NewCustomerContext.CustomerPhoneBooks.Add(customerPhoneBook4);
                    NewCustomerContext.SaveChanges();
                    Console.WriteLine("New customerPhoneBook4 Added Called  : " + customerPhoneBook4.Phone + " With Id :" + customerPhoneBook4.Id);

                }


                if (!string.IsNullOrEmpty(OldExcelCustomer.Governorate))
                {
                    Location Newlocation = new Location();


                    Newlocation.Governorate = OldExcelCustomer.Governorate;
                    Newlocation.Area = string.IsNullOrEmpty(OldExcelCustomer.Area) ? "" : OldExcelCustomer.Area;
                    Newlocation.Block = string.IsNullOrEmpty(OldExcelCustomer.Block) ? "" : OldExcelCustomer.Block;
                    Newlocation.Street = string.IsNullOrEmpty(OldExcelCustomer.Street) ? "" : OldExcelCustomer.Street;

                    Newlocation.Title = string.IsNullOrEmpty(OldExcelCustomer.LocationTitle) ? "" : OldExcelCustomer.LocationTitle;

                    Newlocation.PACINumber = "";

                    Newlocation.Latitude = 47.655406;
                    Newlocation.Longitude = 47.655406;

                    Newlocation.AddressNote = string.IsNullOrEmpty(OldExcelCustomer.Address) ? "" : OldExcelCustomer.Address;
                    Newlocation.AppartmentNo = 0;
                    Newlocation.Building = string.IsNullOrEmpty(OldExcelCustomer.Building) ? "" : OldExcelCustomer.Building;
                    Newlocation.FloorNo = 0;
                    Newlocation.HouseNo = OldExcelCustomer.HouseNo == null ? 0 : OldExcelCustomer.HouseNo;

                    Newlocation.Fk_Customer_Id = NewCustomer.Id;

                    Newlocation.CreatedDate = DateTime.Now;
                    Newlocation.UpdatedDate = DateTime.Now;
                    Newlocation.DeletedDate = DateTime.Now;
                    Newlocation.IsDeleted = false;



                    NewCustomerContext.Locations.Add(Newlocation);
                    NewCustomerContext.SaveChanges();
                    Console.WriteLine("New Newlocation Added Called  : " + Newlocation.Governorate + " With Id :" + Newlocation.Id);

                }


                OldCustomerData oldCustomerData = new OldCustomerData();

                oldCustomerData.LedgerID = OldExcelCustomer.LeaderId;
                oldCustomerData.MajorCode = OldExcelCustomer.MajorCode;
                oldCustomerData.MinorCode = OldExcelCustomer.MainorCode;
                oldCustomerData.PaymentTerm = OldExcelCustomer.PaymentTerm;
                oldCustomerData.ReceivableAccount = OldExcelCustomer.RecivableAccount;
                oldCustomerData.SiteAddress = OldExcelCustomer.SiteAddress;
                oldCustomerData.SiteCode = OldExcelCustomer.SiteCode;
                oldCustomerData.SiteUseCode = "";
                oldCustomerData.AccountNo = OldExcelCustomer.AccountNo;
                oldCustomerData.CreditLimitOverall = OldExcelCustomer.CreditLimitOverall;
                oldCustomerData.CreditLimitTRX = OldExcelCustomer.CreditLimitTrx;
                oldCustomerData.CustomerName = OldExcelCustomer.Name;
                oldCustomerData.CustomerSiteName = OldExcelCustomer.CustomerSiteNo;
                oldCustomerData.CustomerSiteNo = OldExcelCustomer.CustomerSiteNo;
                oldCustomerData.FileNo = OldExcelCustomer.FileNo;
                oldCustomerData.FK_Customer_Id = NewCustomer.Id;

                oldCustomerData.IsDeleted = false;
                oldCustomerData.CreatedDate = DateTime.Now;
                oldCustomerData.UpdatedDate = DateTime.Now;
                oldCustomerData.DeletedDate = DateTime.Now;

                NewCustomerContext.OldCustomerDatas.Add(oldCustomerData);
                NewCustomerContext.SaveChanges();
                Console.WriteLine("New Newlocation Added Called  : " + oldCustomerData.CustomerName + " With Id :" + oldCustomerData.Id);





            }


            Console.ReadKey();

        }
    }
}
